pt = b"Long ago, the four nations lived together in harmony ..."
ct = "200e0d13461a055b4e592b0054543902462d1000042b045f1c407f18581b56194c150c13030f0a5110593606111c3e1f5e305e174571431e"
ct = [int(ct[i:i+2],16) for i in range(0, len(ct), 2)]
r = [ chr(i[0] ^ i[1]) for i in zip(ct, pt) ]
print( "".join(r))