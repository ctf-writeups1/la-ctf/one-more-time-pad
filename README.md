# one-more-time-pad         154



## Description

I heard the onetime pad is perfectly secure so I used it to send an important message to a friend, but now a UCLA competition is asking for the key? I threw that out a long time ago! Can you help me recover it?

file : chall.py

## Analyse

We can see in the code a simple process of xor encryption.

Xor cypher use a key and a message to deliver a cypher text. This encryption work like :
For each character in the message to encode, we take the corresponding char in key, and do a xor
between there bytes representation.

The key seems to be the flag. 

## Method

We can see that we have a pre-configured message "Long ago, the four nations lived together in harmony ...", and in comments, a cypher text "200e0d13461a055b4e592b0054543902462d1000042b045f1c407f18581b56194c150c13030f0a5110593606111c3e1f5e305e174571431e"

As the cipher is a simple xor, we just can xor a message and a cypher text of the same message to obtain the key that encrypt it.

## Implementation

I wrote a simple python script that take the cipher text and original message, split the cipher each 2 chr and int them with hexa to obtain the list of hexa ciphered chars. then, i xor each char with these corresponding key and find original char.

```python
pt = b"Long ago, the four nations lived together in harmony ..."
ct = 
 "200e0d13461a055b4e592b0054543902462d1000042b045f1c407f18581b56194c150c13030f0a5110593606111c3e1f5e305e174571431e"
ct = [int(ct[i:i+2],16) for i in range(0, len(ct), 2)]
r = [ chr(i[0] ^ i[1]) for i in zip(ct, pt) ]
print( "".join(r))
```
